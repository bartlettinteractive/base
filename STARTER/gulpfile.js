const gulp = require('gulp');
const gutil = require('gulp-util');
const sass = require('gulp-sass');
const sassGlob = require('gulp-sass-glob');
const watch = require('gulp-watch');
const rename = require('gulp-rename');
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const fs = require('fs');
const drupalBreakpoints = require('drupal-breakpoints-scss');
const config = require('./example.config');

/**
 * If config.js exists, load that config for overriding certain values below.
 */
function loadConfig() {
  if (fs.existsSync(__dirname + '/./config.js')) {
    config = {};
    config = require('./config');
  }

  return config;
}

loadConfig();

/**
 * This task breakpoint variables.
 */
gulp.task('breakpoints', function () {
  return gulp.src('./STARTER.breakpoints.yml')
    .pipe(drupalBreakpoints.ymlToScss(config.breakpoints))
    .pipe(rename('_breakpoints.scss'))
    .pipe(gulp.dest('./scss'))
})

/**
 * This task generates CSS from all SCSS files and compresses them down.
 */
gulp.task('sass', function () {
  return gulp.src('./scss/**/*.scss')
    .pipe(sassGlob())
    .pipe(sourcemaps.init())
    .pipe(sass({
      noCache: true,
      outputStyle: 'compressed',
      lineNumbers: false,
      loadPath: './css/*',
      sourceMap: true
    })).on('error', function(error) {
      gutil.log(error);
      this.emit('end');
    })
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./css'));
});

/**
 * This task minifies javascript in the js/js-src folder and places them in the js directory.
 */
gulp.task('js', function() {
  return gulp.src('./js/source/*.js')
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./js'));
});

/**
 * Define a task to spawn Browser Sync.
 * Options are defaulted, but can be overridden within your config.js file.
 */
gulp.task('browser-sync', function() {
  if (config.browserSync.enabled) {
    browserSync.init({
      files: ['css/**/*.css', 'js/*.js'],
      port: config.browserSync.port,
      proxy: config.browserSync.hostname,
      open: config.browserSync.openAutomatically,
      reloadDelay: config.browserSync.reloadDelay,
      injectChanges: config.browserSync.injectChanges
    });
  }
});

/**
 * Defines the watcher task.
 */
gulp.task('watch', function() {
  // Watch scss for changes.
  gulp.watch(['scss/**/*.scss'], ['sass']);

  // Watch js for changes.
  gulp.watch(['js/js-src/**/*.js'], ['js']);
});

gulp.task('default', ['breakpoints', 'watch', 'browser-sync']);
